import { useRef, useState } from "react";
import "./Login.css";
import { useDispatch, useSelector } from 'react-redux';
import { setUserdata } from "../redux/actions/productActions";

function Login() {
  const inptRef1 = useRef(null);
  const inptRef2 = useRef(null);
  const submitRef = useRef(null);
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch();
  const handleSubmit = () => {
      if(username.length===0 ){
          alert("ehter username")
      } else if(password.length===0){
        alert("ehter password")
      }
      else{
      dispatch(setUserdata({
        username,
      }));
      localStorage.setItem("username", username);
    }
  };
  const input1KeyDown = (e) => {
    if (e.key === "Enter") {
      console.log("object");
      inptRef2.current.focus();
    }
  };
  const input2KeyDown = (e) => {
    if (e.key === "Enter") {
      console.log("object");
      submitRef.current.focus();
    }
  };
  return (
    <div className="home_wrapper">
        <div className="input_boxes">
      <div className="user_wrapper ">
        <input
          className="user_input"
          placeholder="Enter Username"
          onKeyDown={input1KeyDown}
          ref={inptRef1}
          onChange={(e)=>setUsername(e.target.value)}
        />
      </div>
      <div className="user_wrapper">
        <input
          className="user_input"
          placeholder="Enter Password"
          type="password"
          onKeyDown={input2KeyDown}
          ref={inptRef2}
          onChange={(e)=>setPassword(e.target.value)}
        />
      </div>
      </div>
        <button onClick={handleSubmit} className="submit_btn" ref={submitRef}>Submit</button>
    </div>
  );
}

export default Login;
