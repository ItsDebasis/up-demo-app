import { combineReducers } from 'redux';
import { userReducer } from './userReducer';

const reducers = combineReducers({
    appData: userReducer,
})

export default reducers;


