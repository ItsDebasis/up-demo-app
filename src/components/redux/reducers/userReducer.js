
import {ActionTypes} from '../constants/action-types';

const initialState = {
    userData: {} ,
  };
  
export const userReducer = (state = initialState, action) =>{
    switch (action.type){
        case ActionTypes.SET_USER_DATA:
            return {...state, userData: action.payload}   
        default:
            return state
    }
}
