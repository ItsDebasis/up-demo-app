import {ActionTypes} from '../constants/action-types';

export const setUserdata = (userData) => {
    return{
        type: ActionTypes.SET_USER_DATA,
        payload: userData
    }
}

export const removeUserdata = (userData) => {
    return{
        type: ActionTypes.SET_USER_DATA,
        payload: userData
    }
}
