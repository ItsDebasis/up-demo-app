function NotFound() {
    return (
        <div style={{textAlign:"center"}}>
            <h1>URL not found</h1>
        </div>
    )
}

export default NotFound
