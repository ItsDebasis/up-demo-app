import { useDispatch,  } from "react-redux";
import { removeUserdata } from "../redux/actions/productActions";

import "./Dashboard.css";

function Dashboard({user}) {

  const dispatch = useDispatch();

  const logout =() => {
    dispatch(removeUserdata({}));
    localStorage.setItem("username","");
  }

  return (
    <div className="dashboard">
      <div className="info_container">
        <div 
        onClick={logout}
        className="user_icon">
          <div className="icon_txt">{user.substring(0, 2)}</div>
        </div>
        <div className="name">Welcome {user}</div>
      </div>
      <div className="dash_container">
        <h1>This  is Demo App</h1>
        <h3>Click the user Icon to logout</h3>
      </div>
    </div>
  );
}

export default Dashboard;
