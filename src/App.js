import "./App.css";
import Login from "./components/Login/Login";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from "./components/Dashboard/Dashboard";
import { useSelector } from "react-redux";
import NotFound from "./components/NotFound";

function App() {
  const getUserData = useSelector((state) => state.appData.userData);
  let username = getUserData?.username;
  let user = localStorage.getItem("username");
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact>
            <Router>{user ? <Dashboard user={user}/> : <Login />}</Router>
          </Route>
          <Route component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
